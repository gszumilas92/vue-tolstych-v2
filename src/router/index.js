import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/views/Home.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/o-mnie',
      name: 'about',
      component: () => import(/* webpackChunkName: "about" */ '@/views/About.vue')
    },
    {
      path: '/oferta',
      name: 'offer',
      component: () => import(/* webpackChunkName: "offer" */ '@/views/Offer.vue')
    },
    {
      path: '/cennik',
      name: 'prizes',
      component: () => import(/* webpackChunkName: "prizes" */ '@/views/Prizes.vue')
    },
    {
      path: '/kontakt',
      name: 'contact',
      component: () => import(/* webpackChunkName: "contact" */ '@/views/Contact.vue')
    }
  ]
})
