import Vue from 'vue'
import App from '@/App'
import router from '@/router'
import * as VueGoogleMaps from 'vue2-google-maps'

import 'bootstrap/dist/css/bootstrap-grid.min.css'
import '@/assets/sass/app.sass'

const GOOGLE_API_KEY = 'AIzaSyCXROKKnPzvjFDOOoB83aylNFukWIjlI2Q'

Vue.use(VueGoogleMaps, { load: { key: GOOGLE_API_KEY } })

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
